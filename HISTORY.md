# History (Change Log)

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;For full details, see this repository's Git logs.  

## Table of Contents

 - [Prior to Formal Versioning](#Prior-to-Formal-Versioning)
   - [Saturday, July 9<sup>th</sup>, 2022](#Saturday-July-9supthsup-2022)
     - [Revision 2](#Revision-2)
   - [Saturday, June 4<sup>th</sup>, 2022](#Saturday-June-4supthsup-2022)
     - [Revision 1](#Revision-1)
 - [Outside of Any Version Control](#Outside-of-Any-Version-Control)
   - [Friday, June 10<sup>th</sup>, 2022](#Friday-June-10supthsup-2022)

## Prior to Formal Versioning

### Saturday, July 9<sup>th</sup>, 2022

#### Revision 2

 - HISTORY.md:  
   - Added some notes of/on some past external project discussion.  

### Saturday, June 4<sup>th</sup>, 2022

#### Revision 1

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Initial release.  (🎉!)  Started out with:  

 - README.md:  
   - Added as a new file with some initial contents.  
 - HISTORY.md:  
   - Added as this new file with some initial release-note contents.  

## Outside of Any Version Control

### Friday, June 10<sup>th</sup>, 2022

 - Shared some of my ideas for this project individually on:  
   - The SpaceBattles forums as parts of [this post](https://forums.spacebattles.com/threads/sword-art-online-thread-the-second.293798/page-195?post=84846973#post-84846973) in its ['Sword Art Online -Thread the Second'](https://forums.spacebattles.com/threads/sword-art-online-thread-the-second.293798/) ('sic' with regard to punctuation.)  
   - The Questionable Questing forums in the second part of [this post](https://forum.questionablequesting.com/threads/sword-art-online-bits-and-bytes.4117/page-140#post-5677052) in [its '[NSFW] Sword Art Online Bits and Bytes' thread](https://forum.questionablequesting.com/threads/sword-art-online-bits-and-bytes.4117/).  