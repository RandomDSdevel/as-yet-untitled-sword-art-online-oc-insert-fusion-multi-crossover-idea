# As-Yet-Untitled _Sword Art Online_ OC-Insert Fusion Multi-Crossover Idea

---

_Note:_  This repository is laid out in [my 'Semantic Looseleaf Workspace' format](https://gitlab.com/semantic-looseleaf-workspace/specification).  

---

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;A seed&thinsp;—&thinsp;[ha!](https://swordartonline.fandom.com/wiki/World_Seed)&thinsp;—&thinsp;for an as-yet-unnamed _Sword Art Online_ OC-insert fusion multi-crossover idea.  

## Involved Source Material

 - _Sword Art Online_
 - _Log Horizon_
 - _BOFURI:  I Don't Want to Get Hurt, so I'll Max Out My Defense_
 - _Magical Girl Lyrical Nanoha_
 - _Sailor Moon_ (The manga/_Crystal_ timeline, with some elements from the original anime's alternate continuity.)  

## Additional Inspirations

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;This project is also inspired in whole or in part by and also borrows some elements and takes some cues from the following other works of _Sword Art Online_ fan fiction:  

 - _Unyielding_ by Ikell, hosted both [here](https://www.fanfiction.net/s/13858855) on FanFiction.net and [here](https://archiveofourown.org/works/30743144) on An Archive of Our Own.  
 - _The Moon's Flash Princess_, a _Sailor Moon_ crossover by Neph Champion, with a TV Tropes page listing links to all of its multiple books [here](https://tvtropes.org/pmwiki/pmwiki.php/Fanfic/TheMoonsFlashPrincess).  
 - _Usagi Quest_, a multi-crossover with _Ranma ½_, multiple _Pretty Cure_ series, _Inuyasha_, and _Senki Zesshou Symphogear_ with hints of _Magical Girl Lyrical Nanoha_'s making an appearance to join the ensemble later by Lunaryon, hosted [here](https://forums.sufficientvelocity.com/threads/usagi-quest-ck3-princess-maker-sailor-moon.73457/) on the Sufficient Velocity forums.  
 - _Illusions of Mastery_, a _Magical Girl Lyrical Nanoha_ crossover, also by Neph Champion, hosted [here](https://forums.spacebattles.com/threads/illusions-of-mastery-sword-art-online-magical-girl-lyrical-nanoha.855510/) on the SpaceBattles forums.  
 - _Trapped_, a Kirito gender-bender fic where Suguha has an account way back in the beginning of the whole initial SAO incident and Kirito somehow ends up stuck using it by NihilAsura, hosted both [here](https://lycelia.com/s/iu) on Lycelia.com and [here](https://www.fanfiction.net/s/8641128/) on FanFiction.net, as well as Exalted_Lotus's/Lewd Lotus's [fan rewrite of its first chapter](https://drive.google.com/open?id=1hH09Xnjw1gaEgy7TFXU66WRpvEmhaL99).  

## Contributing

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;I welcome and encourage contributions to this as-yet-untitled _Sword Art Online_ OC-insert fusion multi-crossover idea.  I appreciate any helpful review comments, constructive criticism, and other feedback you might have in mind.  The project's open to issue and merge request submissions.  

## Forking the Repository

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Forking this repository, either to serve as a staging ground for merge requests back here upstream or to create your own derivative project or work (see below in [the next section](#Derivative-Works),) is perfectly fine; just keep [the licensing guidelines belwo](#Licensing) in mind.  

## Derivative Works

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Creating deriviative projects or works of all or part of this project is perfectly fine; again, just keep [the licensing guidelines below](#Licensing) in mind.  I'd in fact welcome and be happy to see somebody doing their own take, in whole or in part, on what I've put up here, particularly since my own version of it even getting its first chapter is non-trivially far off into the future at the moment.  On the off chance that anybody did this, however, I'd appreciate it if they:  

 - Opened a merge request to add a mention of their work to this read-me
 - Otherwise let me know that their work exists by either:  
   - Opening an issue.  
   - Reaching out to me by other means.  

That way, I can note any such derivative works that might come along down here in this read-me.  Also, I'd heavily appreciate it if any prospective authors of such a derivative work would ask before doing so if they wanted to use any of my OCs, at least without changing their names and perhaps other details about them.  

## Community

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;For other project discussion, see its threads on the following external (forum) sites:  

 - [SpaceBattles forums thread](https://forums.spacebattles.com/threads/as-yet-untitled-sword-art-online-oc-insert-fusion-multi-crossover-idea.1091508/)
 - [Sufficient Velocity forums thread](https://forums.sufficientvelocity.com/threads/as-yet-untitled-sword-art-online-oc-insert-fusion-multi-crossover-idea.117657/)
 - [Royal Road forums thread](https://www.royalroad.com/forums/thread/126341)
 - [Questionable Questing forums thread](https://forum.questionablequesting.com/threads/as-yet-untitled-sword-art-online-oc-insert-fusion-multi-crossover-idea.19000/)
 - [BigCloset TopShelf forums thread](https://bigclosetr.us/topshelf/forum-topic/96930/as-yet-untitled-sword-art-online-oc-insert-fusion-multi-crossover-idea)

## Maintainer

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;This project's current maintainer is Bryce 'RandomDSdevel'/'WhenCatsFoxesandWolvesFly' Glover.  

## Licensing

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;This project currently remains unlicensed, but this may change at any time.  Regardless of which license is eventually chosen, it will:  

 - Be a free, libre, and/or open-source license in perpetuity.  
 - Allow derivative works.  
 - Require proper content attribution.  

For the time being, this project's maintainer currently reserves the right to make any and all decisions which would be governed by the terms of any such license.  
