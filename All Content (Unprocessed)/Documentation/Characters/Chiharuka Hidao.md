---
Name:  
 - English:  Chiharuka Tomari Hidao
 - Japanese:  ひだお 千春花 とまり
 - Rōmaji:  Hidao Chiharuka Tomari
Nickname:  Ruka
In-Game Name:  Laika
Age:  TBD
Birthday:  TBD
Sex:  Female
Hair Color:  'Soft, Light, Sandy Golden Brownish-Blonde'
Eye Color:  Brown
Height:  TBD
Weight:  TBD
Affiliation:  'Protagonist Group with Name TBD/TBA'
---

# Chiharuka 'Ruka' Hidao

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;**Chiharuka Tomari Hidao** (ひだお 千春花 とまり, _Hidao Chiharuka Tomari_,) known affectionately by her friends by the nickname 'Ruka' and as 'Laika' in the game Sword Art Online, is one of this work's protagonists.  She's the daughter of a (currently unnamed) Chinese-American father and a (currently also unnamed) Japanese mother.  

## Appearance

### Real World and Sword Art Online

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Chiharuka is a young woman in her teens with brown eyes and soft, light, sandy gold brownish-blonde hair.  She wears her hair short in front with moderate bangs and down to her chin and neck back behind her ears.  She also has a bust which is of notable size, but not one developed _quite_ as expansively as her best friend's.  She also has long legs and a lean build.  

## Personality

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Chiharuka starts out as a sensitive, kind-hearted girl who's loyal to and will stand up for her friends.  Later, though, she gains a bit more of a gutsy edge in how she carries herself after her consciousness gets subsumed by that of a young man of similar age from a separate timeline somewhat like ours.  

## History

### Background

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Chiharuka was born the daughter of a (currently unnamed) Chinese-American father and a (currently also unnamed) Japanese mother.  

### Aincrad Arc

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Chiharuka logged in to play Sword Art Online at the launch of its final public release to join her best friend there, as the latter had taken part as a player in its closed beta test.  Her consciousness gets subsumed by that of a young man of similar age from a separate timeline somewhat like ours quite early on in the game.  

## Skills

### Sword Art Online

 - **Mind's Labyrinth Anchor Palace**&thinsp;—&thinsp;Allows the user to:  

   - Store soul-bound pocket dimensions containing magical simulacra of places they've ascribed personal significance to.  Each such replica exists inside something a bit like a [Temporal Force Field](https://nanoha.fandom.com/wiki/Temporal_Force_Field), except that the resulting boundary maintains the dimensional containment needed to keep them stable.  They wouldn't hold up against extending at least partly into the '[Dimensional Sea](https://nanoha.fandom.com/wiki/Dimension_space) of [the bulk](https://en.wikipedia.org/wiki/Brane_cosmology#Brane_and_bulk) like they do otherwise.  
   - Freely access them at will.  
   - Employ them as portable safe zones and for extra inventory storage space.  

   (Starts out limited to replicas of two locations.)  

   Available copied locations:  

   - Chiharuka's room or whole house.  (Extent TBD.)  
   - The room or whole house (extent also TBD) belonging to the individual from a timeline similar to ours whose consciousness subsumes hers.  

## Partial Inspirations

 - [Chiho Hidaka](https://sekirei.fandom.com/wiki/Hidaka_Chiho) from _Sekirei_ (Relationship:  Partial expy.)  
 - After considering multiple possibilities for it, for her middle name, [Tomari Kurusu](https://ka-shi-ma-shi.fandom.com/wiki/Tomari_Kurusu) from _Kashimashi:  Girl Meets Girl_.  (Also an indirect reference to how [Hazumu Osaragi](https://ka-shi-ma-shi.fandom.com/wiki/Hazumu_Osaragi), the main character from the same series, also undergoes a gender-bend like how the young man of similar age from a separate timeline somewhat like ours whose consciousness subsumes Chiharuka's gets OC-inserted into her life.)  
 - For her in-game name, the wolf character also named Laika from David Clement-Davies's _The Sight_, though I'm not entirely sure if I'm going to further turn Chiharuka into a wolf beastkin like in _Trapped_.  
