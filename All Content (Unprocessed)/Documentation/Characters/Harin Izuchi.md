---
Name:  
 - English:  Harin Izuchi
 - Japanese:  いづち 羽凜
 - Rōmaji:  Izuchi Harin
Nickname:  Rin
In-Game Name:  TBD
Age:  TBD
Birthday:  TBD
Sex:  Female
Hair Color:  Dark Brown
Eye Color:  Brown
Height:  TBD
Weight:  TBD
Affiliation:  'Protagonist Group with Name TBD/TBA'
---

# Harin Izuchi

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;**Harin 'Rin' Izuchi** (いづち 羽凜, _Izuchi Harin_) is [Chiharuka Hidao](All Content (Unprocessed)/Characters/Chiharuka Hidao.md)'s best friend and one of this work's supporting main protagonists.  

## Appearance

### Real World and Sword Art Online

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Harin is a well-endowed young woman in her teens with brown eyes and long, dark brown hair.  

## Personality

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Harin is a cheerful and easygoing girl.  Cosplaying as certain anime, manga, light novel, and video game characters is one minor hobby of hers, though she isn't as absorbed in or obsessed with the pastime as some are.  Rin does have fun doing group cosplay, though, too, including with Chiharuka sometimes.  Harin can also be a bit of a tease at times.  She also has serious and sharp-focused sides to her and can get quite serious when deep 'in the zone,' so to speak.  
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Harin is also a fairly dedicated gamer.  In addition, she's into sports and plays on her school's volleyball team.  

## History

### Background

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;…  (TO-DO)

### Sword Art Online Closed Beta

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;As one of the lucky 1,000 players accepted into Sword Art Online's closed beta test, Harin logged a not-insignificant number of hours of play time.  (_TO DO:_  Figure out how high she got floor-wise before the beta ended.)  

### Aincrad Arc

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;…  (TO-DO)

## Known Equipment

### Sword Art Online Closed Beta and Aincrad Arc

([**Note**] _TO DO:_  Split this into two sections, one for each of the two arcs currently covered here.)  

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Harin is known to favor using a combination of either two\* one-handed curved swords, more specifically katanas as she progresses further into the game, and/or a paired set of long, sharp, retractable claws that telescope to lock at multiple lengths and are suited to both combat and climbing.  

| Name |          Type           |    Acquisition     | Notes |
|:----:|:-----------------------:|:------------------:|:-----:|
| TBD  | One-Handed Curved Sword | Starting Equipment |       |
| TBD  |         Claws           |        TBD         |       |

(_TO DO:_  

 - [\*&thinsp;Also a **note**:]  Decide what katanas Harin dual-wields later&thinsp;—&thinsp;yes, that means Dual Blades doesn't work the same here as it does in canon.  For this work, I was thinking maybe dual-wielding could inflict some kind of stat reduction on players unless they have the Dual Blades skill, which would take it away.  The Dual Blades skill also wouldn't be _entirely_ unique any more, though it'd of course still be non-trivially rare.  Maybe stats other than speed, stat progression, and/or some other, unknown prerequisite affect when it can get unlocked?  Kirito still has his chance at gaining Dual Blades first, naturally.  
 - Figure out what other weapons Harin uses during her tenure as a player and later an adventurer.)  

## Skills

### Sword Art Online

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;…  (TO-DO)

## Partial Inspiration

 - [Uzume](https://sekirei.fandom.com/wiki/Uzume) from _Sekirei_ (Relationship:  Partial expy.)  
