# Items

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;An **Item** is any object in a game world that can be collected by players or non-player characters (NPCs) and stored in their inventories.  
Items range from player equipment&thinsp;—&thinsp;weapons, armor, and various accessories&thinsp;—&thinsp;to potions to ingredients.  Other items are used as materials for crafting, completing quests, or modifying other items or can be interacted with in other ways.  They can be obtained from shops, monsters, quests, or raids.  
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;A particular game's items may be classified by various ranks, which help determine their rarity, stats, and abilities.  Rarer items may prove to be 'soul-bound,' or subject to a 'lock system,' which:  

 - Binds them to the first player who acquires them, 
 - Prevents that player from trading or selling them to anyone else, player or NPC, and
 - Makes it difficult or impossible for other players to use them, at least not to their full potential.  

## Ranks

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Items come labeled as belonging to one of several tiered ranks classifying how strong, durable, rare, etc. they are.  The item ranks available by game are:  

(**TO-DO:**  Decide if these item-ranking systems remain separate or get combined in some way, as well as how they might interact.)  

### Sword Art Online

(**TO-DO:**  List and describe the known item ranks.)  

### _Elder Tale_

#### Normal

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Normal items are commonly sold in shops.  They have the lowest stats for their levels as compared to items in higher ranks and have no magical effects.  That said, they're the cheapest option for Adventurers who use consumable items like arrows.  

#### Magic-class

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Magic-class items are, in many cases, simply Normal-class items that've had up to two magical abilities added to them.  For example, a 'Fire Longsowrd' is a Normal-class 'Longsword' augmented with elemental fire magic properties.  
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;It's possible to purchase Magic-class items from People of the Land who deal in magical goods.  This rank of item also has a high chance of appearing in dungeon treasure chests, too.  Around ten of them can be obtained every adventure, so each member of a party of six can get roughly one or two Magic-class items apiece.  Though Adventurers don't really consider these rare, they do bring them back with them to sell for Gold.  After the Catastrophe, these items get produced in increasingly large quantities.  

#### Production-class

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Production-class items are, as the name implies, crafted by Adventurers using different materials.  Items of this rank can hold 3–5 abilities if a high-quality Normal item was used as a base, giving them a wider range of possible abilities than Magic-class items.  
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Although their stats and ability modifiers are lower than you'd see on Secret- or Artifact-class items (see below,) the abilities you get on those are highly randomized, making it difficult to get what you want.  Many Adventurers thus use Production-class items made to their specifications instead.  
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Items crafted by high-level players are considered luxury goods.  After the Catastrophe, those Adventurers have been researching the use of different magical materials and creation processes.  

#### Secret- or Artifact-class

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Powerful Secret- or Artifact-class items are obtained from dungeons, monster drops, or quests.  Naturally, joining a party is required to get this rank of item.  It takes about two weeks in a dungeon to get a Secret- or Artifact-class piece of equipment to drop.  It's rare for none of a party's members to gets one while it's down in one.  Harcore gamers typically get several as they make their way to level 90, and most Adventurers have at least one or two Secret- or Artifact-class items by that point.  
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;For the most part, getting Secret- or Artifact-class items isn't difficult:  the really time-consuming part is getting one that fits your needs.  For example, getting a 'Secret-class Longbow' is about twenty times harder than getting just any Secret- or Artifact-class item.  An item of this rank that matches its owner's needs is well reputed as a very valuable and reliable partner.  
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Secret- or Artifact-class items have a high chance of being subject to the lock system.  Therefore, once such a 'soul-bound' item's owner is decided, it becomes bound to the player's inventory and nobody else can place it in theirs any more.  Depending on the item, it may also have some or all of its capabilities limited as exclusive for its owner's use.  Careful handling can, however, allow an Adventurer to prevent a Secret- or Artifact-class item from selecting them as its owner; see later on in this article for one method of doing that.  Items of this rank that were originally collected in this way are sometimes referred to as 'clean items' and can be found being traded by auction, but they're typically very expensive.  

#### Fantasy- or Phantasmal-class

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Fantasy- or Phantasmal-class items were the rarest grade of items prior to the Catastrophe, when _Elder Tale_ was still just a game.  Only a limited number of items in this rank exist per server; each expansion pack typically adds around 500 Fantasy- or Phantasmal-class items per server.  Along with having top-grade stats and abilities, Fantasy- or Phantasmal-class items sometimes have other perks like the ability to speak or an in-built AI.  
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Fantasy- or Phantasmal-class equipment can only be acquired through raids or forged from 'phantasmal' material&thinsp;—&thinsp;hence their alternate name&thinsp;—, also obtained through raids.  As such, acquiring this rank of itme requires at least 24 people, the minimum number needed to complete a Rank 1 Raid, and a lot of training and patience.  A Fantasy- or Phantasmal-class item is soul-bound, locked into its owner's inventory, and therefore can't be traded.  Before the Catastrophe, any player who acquired a Fantasy- or Phantasmal-class item would have his or her user name broadcasted across their entire home server, making it impossible for them to conceal the fact that they'd obtained one.  

#### Sacrament-class

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'Sacrament-class' is a completely new item rank which got added as part of the _The Novasphere Pioneers_/_Homesteading the Noosphere_ expansion and thus didn't exist before the Catastrophe.  It's unknown what these items do or what their properties are, and the only individual known to have a Sacrament-class item is Roe2.  

## Types of Items

### Equipment

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Equipment is worn by players and NPCs in order to boost their stats.  Some monsters and Raid Bosses are also capable of donning equipment.  Equipment of varying rank can be dropped by monsters.  
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Most equipment can be re-colored and have a guild logo added to it.  Some guilds<!--, including the D. D. D. and the Black Sword Knights,--> are particularly notable for having all of their equipment follow a running theme.  <!--s/-->At least one guild<!--/The Black Sword Knights/--> is known to have chosen its standard cape's color and design chosen by popular vote amongst its members.  

### Potions and Food Items

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Potions can be used to restore HP or MP or cure status effects.  Chefs can also produce food items that grant temporary buffs when consumed.  

## Drops

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Like gold, items can get dropped by defeated monsters.  Depending on the game, items may or may not enter a player's inventory automatically; if not, then it must be looted from monster corpses.  
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;In dungeons and raids, the loot gained from battle initially becomes the joint property of all of the victorious party's members, appearing in a pile in games where it doesn't get distributed automatically.  In games where loot distribution happens automatically, players can agree on how drops get allocated afterward before entering battle.  
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Different parties and guilds have chosen different systems for allocating dropped equipment.  Some parties' or guilds' leaders give items out to the party or guild member whom they believe they'd be best suited to or who met a specific condition&thinsp;—&thinsp;such as landing the last hit on an enemy or boss, called getting the 'Last-Attack Bonus,' which some games require be honored, at least for boss fights.  Other parties or guilds decide who gets what by group vote or random lottery.  
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Interesting to note is that one player can be designated to collect all loot to give it out later.  This is one way to circumvent soul-bound items immediately getting locked to one particular player's inventory.  

## Equipment Slots

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Equipment can go in slots for the:  

 - body (torso)
 - legs/feet
 - arms
 - hands:  
   - left hand
   - right hand
 - neck and shoulders&thinsp;—&thinsp;e. g.:  cloaks
 - other accessories

as well as in slots on mounts and in bags.  

## Known Weapons

⁝

## Known Armor

⁝

## Known Accessories

⁝

## Other Known Items

⁝

## References

 - [Items | Sword Art Online Wiki | Fandom](https://swordartonline.fandom.com/wiki/Items)
 - [Items | Log Horizon Wiki | Fandom](https://log-horizon.fandom.com/wiki/Items)
