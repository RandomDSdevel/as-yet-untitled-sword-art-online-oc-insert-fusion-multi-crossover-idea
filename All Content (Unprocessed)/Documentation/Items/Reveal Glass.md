---
Name:  Reveal Glass
Game:  Sword Art Online
Item Type:  Miscellaneous
"User(s)":  All SAO Players
---

# Reveal Glass

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;The **Reveal Glass** is a miscellaneous\* item in Sword Art Online.  

\*&thinsp;(_Aside:_  [Possible 'TO-DO'] Should I introduce a different item category and use that for this instead?  This item's (re-)naming inspiration&thinsp;—&thinsp;see under the 'Trivia' section, below&thinsp;—&thinsp;makes it a '[Key Item](https://bulbapedia.bulbagarden.net/wiki/Key_Item).')  

## Description

_A mysterious mirror that reveals the unseen._  
—&thinsp;Sword Art Online base item in-game description text

_A mysterious mirror that reveals the truth._  
—&thinsp;Sword Art Online in-game description text for a particular variant of this item (variant name undecided/TO-DO)

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;The Reveal Glass is a plain rectangular glass mirror with a frame.  The mirror is only slightly smaller than a player's face.  It has various effects depending on whether you have the base item or one of its variants.  

## Effects

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;The base item just functions as a regular mirror.  Like a scant few certain other mirrors in SAO, it also unlocks and serves as an optional alternate trigger for the game's avatar appearance-manipulation system, also accessible from players' system settings menus once enabled.  

### Item Variants

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;…  

([TO-DO] Come up with item variants with effects that:  

 - Forcefully and permanently alter players' avatars to reflect their real-life appearances based on NerveGear camera and calibration data.  Per 'Trivia' below, this corresponds to what _Sword Art Online_ canon's Hand Mirror does.  For this variant, add something like this, adapted from the relevant article in the upstream _SAO_ Fandom wiki, as part of its description on this page:  

   "The mirror was meant to be a way for players to easily see the changes in their own appearance and thus realise that the game had become their only reality."  
 - Unlock players' real-world bodies as an avatar option for them, but without locking them out of the avatar they picked when they first logged in.  Avatar variants can then be selected either using the item or from within the game's system settings menu.  
 - Forcefully and permanently lock a player playing with an avatar of the opposite sex/gender into that sex/gender, making that avatar their real body to start with instead of just a physical glamor.  Their real-world body then becomes inaccessible to them as an avatar.  

   &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;This can be meant to either:  

   - Grant players with gender dysphoria who picked an avatar of the sex they'd prefer to be in real life true relief from it.  
   - Punish players who picked avatars of the opposite sex for ulterior motives so they're stuck that way.  
 - Forcefully and permanently give players opposite-sex/-gender clones of themselves, with copies of all of their original's memories intact.  

   &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Rare; so far, I'm only thinking of having Kirito get this one.  

Effects marked as being permanent can't ever be undone by another appearance-manipulation item, limiting any changes it can make to those which don't overturn the constraints of those first effects being permanent.  Each item variant will also need a name, naturally enough, probably just one similar to what you'd get from putting an item through the enhancement system.)  

## Acquisition

 - The base Reveal Glass is available in certain spots in the floating islands surrounding Aincrad where players first log in.  
 - One of two things happens when Akihiko Kayaba gives his gift out during his announcment in the Town of Beginnings's main plaza square:  
   - If a player hasn't already obtained the base Reveal Glass, then they get the variant of it which changes their avatar to be locked as a copy of their real-world body.  (TO-DO:  Give that variant a name when it gets one along with all the others.)  
   - If a player _has_ obtained the base Reveal Glass already, then the one in their inventory gets changed to one of the other remaining variants.  

## Trivia

 - This item's name is a reference to the [Reveal Glass](https://bulbapedia.bulbagarden.net/wiki/Reveal_Glass) in the _Pokémon_ games starting in Generation <!--Roman numeral five-->&#x2164;.  
 - The variant of this item with the same effects is this continuity's take on _Sword Art Online_ canon's [Hand Mirror](https://swordartonline.fandom.com/wiki/Hand_Mirror).  
 - In _SAO_ canon, mirrors in the virtual world (at least in the SAO game) aren't negatively affected by distance, so it's therefore possible to use them to see distant objects in high detail despite this being impossible in the real world.  The Reveal Glass doesn't share this capability, instead just acting exactly like a real-world mirror in terms of focusing ability.  

## References

 - [Hand Mirror | Sword Art Online Wiki | Fandom](https://swordartonline.fandom.com/wiki/Hand_Mirror)
 - [Reveal Glass&thinsp;—&thinsp;Bulbapedia, the community-driven Pokémon encyclopedia](https://bulbapedia.bulbagarden.net/wiki/Reveal_Glass)
